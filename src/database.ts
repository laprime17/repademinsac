import Dexie from 'dexie';




export interface IComponente{
	id?:number;
    idsistema : number;
    descripcion : string;
    img : string;
}
export interface IZona{
	id?:number;
    idcomp : number;
    descripcion : string;
}
export interface IInspeccionZona{
    orden:number
    idndt_insp_comp: number;
	id?:number;
    idzona : number;
    zona : string;
    tecnica_ndt :string;
    fecha_ult_insp :string;
    estado_previo:string;
    fecha : string;
    condicion?: string;
    comentarios :  string;
    foto? : string;
    icono : string;

}
// export interface IFisura{
// 	id?:number;
//     idinspzona : number;
//     nombre : string;
//     long : number;
//     observacion : string;
//     foto?:string;
//     condicion : string;
//     tipo:string;
//     fecha:string;
//     icono:string;
//     fl_insp:string;
//      estado_sinc:string;
// }
export class Fisura{
    id?:number;
    idinspzona : number;
    nombre : string;
    long : number;
    observacion : string;
    foto :string;
    condicion : string;
    tipo:string; //n=nuevo,r=reaparece,e=existente(obtenido de histórico)
    fecha:string; //fecha de inspecciòn anterior
    icono:string;
    fl_insp:string;
    estado_sinc:string;

    constructor(idinspzona:number,nombre:string,long:number,observacion:string,foto:string,tipo:string,condicion:string,fecha:string,icono:string,fl_insp:string,estado_sinc:string){

        this.idinspzona = idinspzona;
        this.nombre = nombre;
        this.long =long;
        this.observacion = observacion ;
        this.condicion = condicion;
        this.fecha = fecha;
        if(tipo) this.tipo = tipo;
        this.icono = icono;
        this.fl_insp = fl_insp;
        this.estado_sinc = estado_sinc;
        if(foto) this.foto = foto;
    }
    save()
    {
        console.log('guardar fisura',this);
        return db.tfisuras.add(this);
    }
    static saveedit(id:number,objecto:any)
    {
        return db.tfisuras.update(id,{
            long:objecto.long,observacion:objecto.observacion,condicion:objecto.condicion,fecha:objecto.fecha,fl_insp:objecto.fl_insp,icono:objecto.icono
        });
        
    }
     static getfisuras_by_idinspzona(idinspzona_:number,tipo_:string)
    
    {
        
        if(tipo_== 'n')
        {
            return db.tfisuras.where('tipo').notEqual('e').toArray();
        }   
        else
        {
            return db.tfisuras.where({idinspzona: idinspzona_,tipo: tipo_}).toArray();
        }
        
    }
    static getbyid(id:number)
    {
        let Fis:Promise<Fisura>= db.tfisuras.get(id)
       return Fis;
    }
}
export class InspeccionComponente
{

    id:number;
    equipo:string;
    idcomponente: number;
    componente: string;
    fecha:string;
    constructor(id:number,equipo:string,idcomponente:number,componente:string,fecha:string)
    {

        this.id = id;
        this.equipo = equipo;
        this.idcomponente = idcomponente;
        this.componente = componente;
        this.fecha = fecha;
    }
     save()
    {
        
        return db.tinspeccionComp.add(this);
    }
}
export class InspeccionZona implements IInspeccionZona
{
    orden:number;
	idndt_insp_comp: number;
    id?:number;
    idzona : number;
    zona : string;
    tecnica_ndt :string;
    fecha_ult_insp :string;
    estado_previo:string;
    fecha : string;
    condicion: string;
    comentarios :  string;
    foto : string;
    icono : string;

    constructor(orden:number,idndt_insp_comp:number,idzona:number,zona:string,tecnica_ndt:string,fecha_ult_insp:string,estado_previo:string,fecha:string,comentarios:string,foto:string,icono:string){
        this.orden = orden;
        this.idndt_insp_comp = idndt_insp_comp;
        this.idzona = idzona;
        this.zona = zona;
        this.tecnica_ndt = tecnica_ndt;
        this.fecha_ult_insp = fecha_ult_insp;
        this.estado_previo = estado_previo;
        this.fecha = fecha;
        this.condicion = "default";
        this.comentarios=comentarios;
        if(foto)this.foto = foto;
        this.icono = icono

    
    }

    save()
    {
    	
    	return db.inspeccionZona.add(this);
    }
    static all()
    {
    	return db.inspeccionZona.orderBy('id').toArray();
    }

    static getbyid(id:number)
    {
       return db.inspeccionZona.get(id)
    }
}
export class InspeccionesAppDB extends Dexie{
    inspeccionZona : Dexie.Table<IInspeccionZona,number>;
    tfisuras : Dexie.Table<Fisura,number>;
	tinspeccionComp : Dexie.Table<InspeccionComponente,number>;
	constructor(){
		super("InspeccionesAppDB");
		this.version(1).stores({
            inspeccionZona: '++id,idndt_insp_comp,idzona,zona,tecnica_ndt,fecha_ult_insp,estado_previo,fecha,condicion,comentarios,foto,icono,estado_sinc',
            tfisuras: '++id,idinspzona,nombre,long,observacion,foto,tipo,condicion,fecha,icono,fl_insp,estado_sinc',
            tinspeccionComp: 'id,equipo,idcomponente,componente,fecha'
		});
        this.inspeccionZona.mapToClass(InspeccionZona);
        this.tfisuras.mapToClass(Fisura);
		this.tinspeccionComp.mapToClass(InspeccionComponente);
	}

}
export let db = new InspeccionesAppDB();
