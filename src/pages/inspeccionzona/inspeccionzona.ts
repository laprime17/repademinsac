import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InspeccionZona} from '../../database';
import {InspeccionComponente} from '../../database';
import {DetinspzonaPage} from '../detinspzona/detinspzona';

/**
 * Generated class for the InspeccionzonaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inspeccionzona',
  templateUrl: 'inspeccionzona.html',
})
export class InspeccionzonaPage {
	ListaInspecciones: any;  
	detallePage = DetinspzonaPage;
	
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let insComp = new InspeccionComponente(1,'SH012',23043,'STICK','2017-10-18');
        insComp.save();


        let inszona1 = new InspeccionZona(1,1,1,"PLANCHA SUPERIOR",'VT','2017-01-01','seguimiento','2017-09-29','','-','arrow-dropright-circle');
        let inszona2 = new InspeccionZona(2,1,2,"PLANCHA INFERIOR",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona3 = new InspeccionZona(3,1,3,"PLANCHA LATERAL DERECHO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona4 = new InspeccionZona(4,1,4,"PLANCHA LATERAL IZQUIERDA",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona5 = new InspeccionZona(5,1,5,"PLANCHA 'U' SUPERIOR",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona6 = new InspeccionZona(6,1,6,"OREJAS DE MONTAJE A BOOM – LADO DERECHO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona7 = new InspeccionZona(7,1,7,"OREJAS DE MONTAJE A BOOM – LADO IZQUIERDO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona8 = new InspeccionZona(8,1,8,"OREJA DE MONTAJE DE STICK CYLINDER – LADO DERECHO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona9 = new InspeccionZona(9,1,9,"OREJA DE MONTAJE DE STICK CYLINDER – LADO IZQUIERDO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona10 = new InspeccionZona(10,1,10,"PIN UNION BOOM - STICK",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona11 = new InspeccionZona(11,1,11,"PIN STICK CYLINDER LADO VASTAGO – DERECHO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');
        let inszona12 = new InspeccionZona(12,1,12,"PIN STICK CYLINDER LADO VASTAGO – IZQUIERDO",'VT','2017-01-01','seguimiento','2017-09-30','','-','arrow-dropright-circle');

        inszona1.save();
        inszona2.save();
        inszona3.save();
        inszona4.save();
        inszona5.save();    
        inszona6.save();
        inszona7.save();
        inszona8.save();
        inszona9.save();
        inszona10.save();
        inszona11.save();
        inszona12.save();

  		
  }

   	ionViewDidLoad() {	
   	this.mostrar();
  	}

 	mostrar(){
	InspeccionZona.all()
			  .then((data)=>{
			  this.ListaInspecciones = data;
			  console.log("Lista insp",this.ListaInspecciones)	;
			  });
 	}
 	goToPage_DetZona(id:number) {
    this.navCtrl.push(DetinspzonaPage,{
    idinspzona: id
	});
  }

}
