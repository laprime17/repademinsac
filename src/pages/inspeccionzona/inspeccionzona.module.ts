import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InspeccionzonaPage } from './inspeccionzona';

@NgModule({
  declarations: [
    InspeccionzonaPage,
  ],
  imports: [
    IonicPageModule.forChild(InspeccionzonaPage),
  ],
})
export class InspeccionzonaPageModule {}
