import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FisuraPage } from '../fisura/fisura';
import { FisuradetPage } from '../fisuradet/fisuradet';
import { InspeccionZona } from '../../database';
import { Fisura } from '../../database';
/**
 * Generated class for the DetinspzonaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detinspzona',
  templateUrl: 'detinspzona.html',
})
export class DetinspzonaPage {
	// fisuraPage = FisuraPage;
	det_inspZona: InspeccionZona;
  ListaFisuras_nuevas:any = new Array();
  ListaFisuras_exis:any = new Array();
	idinspzona : number;
  tipo_fisura : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.idinspzona = navParams.get('idinspzona');
    let f1 =  new Fisura(this.idinspzona,'F1',60,"Fisura crece 20mm","sin foto","e","seguimiento",'2017-08-31','close','false','nosinc'); 
    
    f1.save();
   

    let f2 =  new Fisura(this.idinspzona,'F2',115,"Fisura crítica, se requiere notificación","sin foto","e","critico",'2017-08-31','close','false','nosinc'); 
    f2.save();

    let f3 =  new Fisura(this.idinspzona,'F3',25,"Fisura nueva","sin foto","e","seguimiento",'2017-08-31','close','false','nosinc'); 
    f3.save();

    let f4 =  new Fisura(this.idinspzona,'F4',60,"Fisura reaparece, se reporta a M8 para reparación  en PM","sin foto","e","seguimiento",'2017-08-31','close','false','nosinc'); 
    f4.save();


  }

  ionViewDidLoad() {
        this.tipo_fisura="nuevas";
	  	InspeccionZona.getbyid(this.idinspzona)
                		.then((data)=>{
              		      this.det_inspZona = <InspeccionZona>data;
              		  });

      Fisura.getfisuras_by_idinspzona(this.idinspzona,'n')
                    .then((data)=>{
                        this.ListaFisuras_nuevas = data;
                        console.log('LISTA DE FISURAS',this.ListaFisuras_nuevas);
                    });

      Fisura.getfisuras_by_idinspzona(this.idinspzona,'e')
                    .then((data)=>{
                        this.ListaFisuras_exis = data;
                        console.log('LISTA DE FISURAS existentes',this.ListaFisuras_exis);
                    });
  }
 
 gotoPage_nuevaFisura(idinspzona_:number) {
    this.navCtrl.push(FisuraPage,{
    idinspzona: idinspzona_,
    parentPage: this
  });
  }

   gotoPage_editFisura(id:number) {
    this.navCtrl.push(FisuradetPage,{
    idfisura: id,
    parentPage: this
  });
  }

}
