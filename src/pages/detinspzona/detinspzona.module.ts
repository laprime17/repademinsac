import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetinspzonaPage } from './detinspzona';

@NgModule({
  declarations: [
    DetinspzonaPage,
  ],
  exports: [
    DetinspzonaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetinspzonaPage),
  ],
})
export class DetinspzonaPageModule {}
