import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Fisura } from '../../database';
import {DetinspzonaPage} from '../detinspzona/detinspzona';
/**
 * Generated class for the FisuraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 
@IonicPage()
@Component({
  selector: 'page-fisura',
  templateUrl: 'fisura.html',	
})
export class FisuraPage {

	model : Fisura;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.model =  new Fisura(navParams.get('idinspzona'),'F',0,"Nueva","sin foto","n","default",'2017-10-22','close','false','nosinc'); 
    

  }

  ionViewDidLoad() {

   
    
    //console.log('fisura 1',JSON.stringify(this.model));
    
  }
  save()
  {
    this.model.save();
    this.navParams.get("parentPage").ionViewDidLoad();
    this.navCtrl.pop();
  }

}
