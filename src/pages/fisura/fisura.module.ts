import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FisuraPage } from './fisura';

@NgModule({
  declarations: [
    FisuraPage,
  ],
  imports: [
    IonicPageModule.forChild(FisuraPage),
  ],
})
export class FisuraPageModule {}
