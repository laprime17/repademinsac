import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { InicioPage } from '../inicio/inicio';
import { InspeccionzonaPage } from '../inspeccionzona/inspeccionzona';
import { DetinspzonaPage } from '../detinspzona/detinspzona';
import { FisuraPage } from '../fisura/fisura';
import { HistorialPage } from '../historial/historial';
import { SincronizacionPage } from '../sincronizacion/sincronizacion';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InspeccionzonaPage;
  /*tab1Root = DetinspzonaPage;*/
  tab2Root = HistorialPage;
  tab3Root = SincronizacionPage;

  constructor() {

  }
}
