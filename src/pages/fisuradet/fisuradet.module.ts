import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FisuradetPage } from './fisuradet';

@NgModule({
  declarations: [
    FisuradetPage,
  ],
  imports: [
    IonicPageModule.forChild(FisuradetPage),
  ],
})
export class FisuradetPageModule {}
