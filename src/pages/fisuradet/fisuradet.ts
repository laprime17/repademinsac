import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Fisura } from '../../database';
import {DetinspzonaPage} from '../detinspzona/detinspzona';
import {Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the FisuradetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fisuradet',
  templateUrl: 'fisuradet.html',
})
export class FisuradetPage {

 	 idfisura : number;
 	 fisuraActual : Fisura;
   condicionAnterior:string;
   fechaAnterior:string;
   longAnterior:number;
   observacionAnterior:string;
   // camera :any;
   imagen : string;

   options :CameraOptions = {
     quality:100,
     destinationType : this.camera.DestinationType.DATA_URL,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType:this.camera.MediaType.PICTURE
   };

   constructor(public navCtrl: NavController, public navParams: NavParams,private camera:Camera) {
     // this.camera = camera_;
   	 this.idfisura= this.navParams.get('idfisura');
 	 
   }

   ionViewDidLoad() {
   		Fisura.getbyid(this.idfisura)
   		      .then((data)=>{
   		      this.fisuraActual = data;
   	          this.condicionAnterior = this.fisuraActual.condicion;
   	          this.fechaAnterior = this.fisuraActual.fecha;
   	          this.longAnterior = this.fisuraActual.long;
   	          this.observacionAnterior = this.fisuraActual.observacion;
   		        console.log("editar fisura",this.fisuraActual);

   		        
   		      });

   }
   guardar_editFisura()
   {
     let objeto= { 
       long: this.fisuraActual.long,observacion: this.fisuraActual.observacion,condicion: this.fisuraActual.condicion,fecha: this.fisuraActual.fecha,fl_insp:true,icono:'time'
     };
     console.log('params to edit',objeto);

     Fisura.saveedit(this.fisuraActual.id,objeto)
      .then(function (updated) {
        if (updated)
        {
        	    	console.log ("Friend number 2 was renamed to Number 2");
        		
        }	
        else
          console.log ("Nothing was updated - there were no friend with primary key: 2");
        
       	


      });
        this.navParams.get("parentPage").ionViewDidLoad();
               this.navCtrl.pop();
   }
   async getFoto():Promise<any>  
   {  
     
     try{

         this.imagen = await this.camera.getPicture(this.options);
         this.imagen = 'data:image/jpeg;base64,'+this.camera;
     }
     catch(e)
     {
       alert(e);
     }
   }
   regresar()
   {
     this.navCtrl.pop();
   }

}
